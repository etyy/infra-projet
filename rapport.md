# nowledgeable | Plateforme de cours et exercices interactifs 
***Contexte***
<p style="text-align:justify;">L'entreprise nowledgeable a pour but de proposer une plateforme web pédagogique interactive et collaborative dans le domaine des sciences, de la programmation et l'intelligence artficielle afin de favoriser la collaboration entre les étudiants et les élèves.</p>
<p style="text-align:justify;">Pour se démarquer de la concurence sur le marché, nowledgable propose aux enseignants d'avoir des exercices prêts à l'emploi directement accessible via le navigateur. Dans les domaines de la programmation et du DevOps. Mais aussi de proposer des tests et d'avoir un suivi des progrès des étudiants. Les élèves auront des exercices avec une correction automatique qui leurs permettront d'identifier immédiatement leurs erreurs. Il y aura la possibité de faire des auto-corrections entre élèves sur les parties théorique.</p>  
<p style="text-align:justify;">La plateforme sera disponible en Europe, en Amérique et en Afrique dans des écoles partenaires.</p>  

___

## **1 - Analyse du problème**

### *Expression des problèmes métiers*  
> Afin de mener à bien cet appel d'offre, voici une liste non exhaustif des problèmes métiers auxquels ont doit faire face. Ils vont permettrent de réaliser une proposition de solution : 
<div align="center">

| Description du problème |
| :---:    |
| Créer/Modifier/Utiliser des exercices |
| Faire/Commenter les exercices dans le navigateur |
| Environnement propre à chaque exercices |
| Statistiques/Dashboard des réponses des élèves |
| Gestion des droits (Élèves != Enseignants)|
| Accès depuis un navigateur |
| Persistence des données |
| Mise à jour en temps réel |
| OAuth Google/Github/Gitlab |
</div>

### *Besoins en terme de temps de réponse*
> On estime qu'un utilisateur qui patiente trop longtemps sur un site web abandonne leur visite si une page met entre 1 et 2 secondes à s'afficher.
<p style="text-align:justify;">C'est pourquoi en temps de réponse inférieur à 1 seconde est envisagé sur l'ensemble de la navigation de l'application web. À contrario lors de la correction d'un exercice par exemple, on peut satisfaire au maximum entre 1 et 5 secondes.</p>

### *Besoins en terme de volumétrie*
<p style="text-align:justify;">Au lancement de l'application on obtient un marché d'environ 100 écoles en Europe, 20 en Afrique et 120 en Amérique. En moyenne il y a 500 élèves/enseignants par école. On estime couvrir dans la finalité plus de 1000 écoles par continent.</p>  

> `140 * 500 = 70 000` *utilisateurs maximum lors du lancement du projet*  
`3000 * 500 = 1 500 000` *utilisateurs maximum lors de la finalité du projet*

___

## 1.2 Proposition d'une solution

### *Diagramme d'architecture globale*
> Suite à l'exposition des problèmes nous pouvons remarquer un taux élévé d'utilisateurs et de fonctionnalités. Pour répondre à tous ces besoins, nous allons utiliser une architecture dites "micro services" afin de permettre le développement, le maintien et le maintien de l'infrastructure plus simple. Pour ce faire on déploi ces micro services sur un cluster Kubernetes gérer par Google Cloud Plateform en mode autopilot afin de ne pas gérer le scaling vertical.
<p style="text-align:justify;">Voici un diagramme présentant l'ensemble des parties du système et leur différentes interactions :</p>

![](diagram.png)


### *Liste des utilisateurs et leurs rôles*
> Afin de démontrer les fonctionnalités de l'application voici un tableau récapitulatif des rôles/droits des utilisateurs :
<div align="center">

| Utilisateurs | Rôles |
| :---    | ---:    |
| Administrateur    | Accès à toutes les ressources de l'application  |
| Enseigants    | Utiliser/modifier des exercices. Accès dashboard statistiques des élèves. Affecter un élève dans un cours. |
| Élèves | Réaliser d'un exercice, mettre des commentaires |
| Développeur | Accès en mode Debug à l'application. Ne peut pas voir les comptes|
| Externe | Peut lire un cours, ne peut pas modifier/créer/faire |
| Visiteur | Navigation/Création de compte | 

</div>

### *User stories*
> Les user stories permettent d'avoir une description simple d'une fonctionnalitée ou d'une attente au niveau d'un produit/application.

* **En tant qu'** enseignants
**je veux** pouvoir créer un cours de python facilement **pour que** mes élèves apprennent vite et avoir un suivis de leur avancement.

* **En tant qu'** élève
**je veux** pouvoir faire un exercice de programmation directement en ligne **pour** avoir une auto-correction à chaque étape.

* **En tant que** visiteur
**je veux** une solution d'apprentisage en ligne accesible à tous **pour** que mes élèves n'ont à rien installé.

### *Typologie des dispositifs clients*
<p style="text-align:justify;">On estime que pour apprendre la programmation dans le cadre scolaire, il faut être munni d'au moins un ordinateur portable/fixe. C'est pourquoi on propose l'application via navigateur web. Cela reste plus pratique et plus simple pour apprendre et à configurer. Cette typologie sera unique pour le moment, et si la demande pour une application mobile est forte, on pourra ajouter cette fonctionnalitée.</p>  

___

## 1.3 Architecture 

### *Les rôles des éléments*
> Se réferer au schéma de l'architecture plus haut. 
<p style="text-align:justify;">Suite à la définition de l'architecture et des solutions envisagées, on va pouvoir détailler son rôle.</p>

* Global LoadBalancer : `Il permet de localiser le client, en Europe/Afrique/Amérique afin de rediriger vers le bon cluster`  

* Cluster GKE Autopilot : `Un cluster kubernetes gérer par Google Cloud, on ne gère pas les nodes à l'intérieur. Il fait du multi-zones dans sa région.`
* Service/Pods : `Les conteneurs des micro-services de l'application ex: Service d'authentification, Service de création/modification d'exercice, Service Dashboard enseignant, Service de correction...`  
* Stockage persistent : `Allou un espace de stockage ou le pod peut venir récupérer des informations autre part que le cluster de base de données`
* Workflow CI/CD : `Build le code dans le cloud, le test et le déploie sur nos infrastructureq. En fonction d'un commit ou d'un tag on créer un déploiement sur nos cluster dev ou staging, et prod.`
* Stack Monitoring/Log : `Héberger sur chaque projet : Prod/Staging/Dev permet de monitorer toute notre infrastructure et d'avoir des logs centralisé puisque GCP propose cela`
* Authentification Oauth : `Permet d'authentifier nos utilisateurs via un service tier reconnue tel que google, github, gitlab`
* Cluster Prosgres : `Cluster de stockage de nos données essentiels`
  


### *Stockage*
> On retrouve deux types de stockage : NoSQL et SQL.
<p style="text-align:justify;">Pour récuperer les différentes métriques de nos clusters, on utilise une base de données prometheus nosql, pour toutes nos autres données on utilise une base de données SQL Postgres</p>

## 1.4 Analyse de coûts 
> Vu qu'on utilise une solution scalable le prix varie en fonction de l'utilisation des utilisateurs.

[Référence du pricing GCP](https://cloud.google.com/products/calculator "GCP Pricing")


<p style="text-align:justify;"></p>

<div align="center">

| Utilisateurs | €/Mois de toutes les solutions GCP incluses |
| :---:    | :---:    |
| 1000    | 900 €    |
| 10 000 | 9 000 € |

</div>

___

## *Etienne Loutsch - Rapport d'architecture*